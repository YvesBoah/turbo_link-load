progressBar ={

 loadedElmt : 0,
 countElmt : 0,

 init : function(){
 	var that = this;
 	this.countElmt = $('img').length;

 		 
			 	// Construction et Ajout De Progress Bar
	var $progressBarContainer = $('<div/>').attr('id', 'progress-bar-container');
	$progressBarContainer.append($('<div/>').attr('id', 'progress-bar'));
	$progressBarContainer.appendTo($('body'));

				// Ajout de Containers des elmts
 	var $container = $('<div/>').attr('id', 'progress-bar-elements');
 	$container.appendTo($('body'));
 	// Parcours des éléments à Prendre en compte pour le chargement
 	$('img').each(function(){
 			$('<img/>')
 			.attr('src', $(this).attr('src'))
 			.on('load error', function(){
 				that.loadedElmt++;
 				that.updateProgressBar();
 			})
 			.appendTo($container);
 	});

 },

 updateProgressBar : function(){

 	$('#progress-bar').stop().animate({
 		'width': (progressBar.loadedElmt/progressBar.countElmt)*100 + '%'},200 , 'linear' , function(){
 			if (progressBar.loadedElmt == progressBar.countElmt) {
 				setTimeout(function(){
 					$('#progress-bar-container').animate({
 						'top' : '-8px'
 					},function(){
 						$('#progress-bar-container').remove();
 						$('#progress-bar-elements').remove();
 					});
 				},500);
 			}
 	});

 }

};

	progressBar.init();